const express = require('express');
const app = express();

app.use('/', express.static(__dirname + "/wwwroot"))

app.listen(8000, function () {
    console.log('Exemple app sur 8000');
})
