const http = require("http");
const server = http.createServer(NewClientCallback);
server.listen(8080);

const url = require('url');



function NewClientCallback(request, response) {
    console.log("New Client Connection");

    if (url.parse(request.url).pathname == '/zipcode') {
        var url_parts = url.parse(request.url, true);
        console.log(url_parts.query);
    }

    const myTown = url_parts.query.city;

    const towns = {
        "Bruxelles": {
            "city": "Bruxelles",
            "zipcode": "1000"
        },
        "Ixelles": {
            "city": "Ixelles",
            "zipcode": "1050"
        },
        "Woluwe": {
            "city": "Woluwe",
            "zipcode": "1200"
        }
    };

    response.writeHead(200, {
        'content-type': 'text/json'
    });

    response.end(JSON.stringify(towns[myTown]));

}