const http = require("http");

const server = http.createServer(NewClientCallback);

server.listen(8080);

function NewClientCallback(request, response) {
    console.log("New Client Connection");

    const time = {};
    const heure = new Date();
    time.heures = heure.getHours();
    time.minutes = heure.getMinutes();
    time.secondes = heure.getSeconds();
    
    response.writeHead(200, { 'content-type': 'text/json' });
    
    response.end(JSON.stringify(time));

}