// node as server for http client
// using express package
const express = require("express");
const app = express();

// manage / route
app.use("/", express.static(__dirname + "/wwwrootconvert"));

// listenning on port 8000
app.listen(8000);

// declare a variable for exchange
let rate = 0;

// node as client of api
// using https package
const https = require("https");
let rawData = "";

const requestToSend = {
    host: "api.exchangeratesapi.io",
    port: 443,
    path: "/latest"
};




app.get("/convert", function (request, response) {

console.log(request.query.devise);

    response.setHeader("Content-Type", "text/html");
    const toConvert = request.query.amount;
    https.get(requestToSend, receiveResponseCallback);

// data et endsont 2 mots clés d'événements
function receiveResponseCallback(responseFromServer) {
    responseFromServer.on("data", chunk => {
        rawData += chunk;
    });
    responseFromServer.on("end", function (chunk) {
        let ratesJSON = JSON.parse(rawData);
        // assign rate
        rate = ratesJSON.rates[request.query.devise];

        // use rate
        response.send(toConvert + "euros is " + (toConvert * rate) + request.query.devise);
    });
}
});