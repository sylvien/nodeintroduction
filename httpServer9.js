// utilisation package http
const http = require("http");

// créer serveur
const server = http.createServer(NewClientCallback);

// définir port d'écoute sur le serveur
server.listen(8080);


// //////////////
// node as server
// //////////////
function NewClientCallback(requestFromClient, responseToClient) {
        // //////////////
        // node as client
        // //////////////
        let rawData = "";
        let vue = "";

        const requestToSend = {
            host: "api.irail.be",
            port: 80,
            path: "/liveboard/?id=BE.NMBS.008812005&lang=fr&format=json"
        };

        http.get(requestToSend, receiveResponseCallback);

        function receiveResponseCallback(responseFromServer) {
            responseFromServer.on("data", chunk => {
                rawData += chunk;
            });
            responseFromServer.on("end", function () {

                const dataJson = JSON.parse(rawData);

                const nbDeparts = dataJson.departures.number;
                const station = dataJson.station;

                for (let index = 0; index < nbDeparts; index++) {
                    let depart = "";
                    
                    const destination = dataJson.departures.departure[index].station;

                    const heure = new Date((dataJson.departures.departure[index].time) * 1000);
                    depart = heure.getHours() + ":" + heure.getMinutes();
                    const retard = (dataJson.departures.departure[index].delay)/60;

                    const voie = dataJson.departures.departure[index].platform;

                    // vue += "<li>Train vers " +
                    // destination + " a" + depart
                    // ": voie " +
                    // voie + "</li>";

                    vue += "<tr><td style='border:1px solid #333'>"+destination+"</td><td style='border:1px solid #333'>"+depart+"</td><td style='border:1px solid #333'>"+voie+"</td><td style='border:1px solid #333'>"+(retard!=0? retard+" min.":"")+"</td></tr>"
                }



                responseToClient.writeHead(200, {
                    'content-type': 'text/html'
                });
        
                responseToClient.end("<h1 style='text-align:center'>"+station+"</h1><table style='width:80vw; margin:0 auto;text-align:center; border:1px solid #333'><tr><th>Destination</th><th>Depart</th><th>Voie</th><th>Retard</th></tr>"+vue+"</table>");
                // responseToClient.end("<ul>"+vue+"</ul>");
            });
        }

    }