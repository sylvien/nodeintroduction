// f8-lancer f9-stop (ctrl+s f8)

// créer une variable qui « requiert » le package ‘http’
const http = require("http");

// instancier le serveur :
const server = http.createServer(NewClientCallback);

// préciser le port TCP sur lequel le serveur va écouter
server.listen(8080);

// envoyer dans la console une ligne pour stipuler que le serveur est démarré :
console.log("Server running at http://127.0.0.1:8080/");

function NewClientCallback(request, response) {
    console.log("New Client Connection");

    const jsonTry = { 
        "nom": "sylvie",
        "age": 30, 
        "mots": {
            "mot1": "sds",
            "mot2": "qsd"
        }
    };
    response.writeHead(200, {
        'content-type': 'text/json'
    });
    response.end(JSON.stringify(jsonTry));

}