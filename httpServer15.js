const MongoClient = require('mongodb').MongoClient;
const url = 'mongodb://localhost:27017/';

MongoClient.connect(url, {useNewUrlParser: true, useUnifiedTopology: true},
    function (errors, dbmongo) {
        console.log('Connected correctly to mongo db');
        const dbLibrary = dbmongo.db("Library");
        // dbLibrary.collection("Books").findOne({}, function (err, result) {
        //    console.log(result); 
        //    dbmongo.close();
        // });
        dbLibrary.collection("Books").find({}).toArray(function (err, result) {
            console.log(result); 
            dbmongo.close();
         });
    });
