const express = require("express");
const app = express();

app.use("/", express.static(__dirname + "/wwwrootzip"));

app.listen(8000);


const filestream = require('fs');
// get a string contening file content
const fileContent = filestream.readFileSync('code-postaux-belge.json');
// get it as Json 
const zipCodes = JSON.parse(fileContent);
console.log(zipCodes);



app.get("/zipcode", function (request, response) {
    response.setHeader("Content-Type", "text/json");
    let index = 0;
    const zipJSON = {};
    let trouve = true;

    while (zipCodes[index].city != request.query.city && index < zipCodes.length-1) {
        index++;
    }

    if (zipCodes[index].city != request.query.city) {
        trouve = false;
    }

    zipJSON.zipcode = zipCodes[index].zip;
    zipJSON.city = zipCodes[index].city;

    response.send(trouve ? JSON.stringify(zipJSON) : "connais pas");
});