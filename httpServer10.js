const express = require('express');
const app = express();

app.listen(8000, function () {
    console.log('Exemple app sur 8000');
})

app.get('/', function (request, response) {
    response.setHeader('Content-Type', 'text/html');
    response.send("<strong>Hello World</strong>");
});

app.get('/books', function (request, response) {
    response.setHeader('Content-type', 'text/html');
    response.send("<strong>Liste des livres</strong>");
});

app.get('/whattimeisit', function (request, response) {
    const date = new Date();
    response.setHeader('Content-Type', 'text/json');
    let timeJSON = {};
    timeJSON.heures = date.getHours();
    timeJSON.minutes = date.getMinutes();
    timeJSON.secondes = date.getSeconds();
    response.send(JSON.stringify({timeJSON }))
}

);