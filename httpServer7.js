const http = require("http");
const server = http.createServer(NewClientCallback);
server.listen(8080);

const url = require('url');


// pour lire un fichier (le synchrone est bloquant jsq à reception de réponse)
const filestream = require('fs');
const fileContent = filestream.readFileSync('code-postaux-belge.json');
const zipCodes = JSON.parse(fileContent);



function NewClientCallback(request, response) {
    console.log("New Client Connection");

    let search = "";
    let index = 0;
    let trouve = true;

    if (url.parse(request.url).pathname == '/zipcode') {
        var url_parts = url.parse(request.url, true);

        search = url_parts.query;

        while (zipCodes[index].city != search.city && index < zipCodes.length-1) {
            index++;
        }

        if (zipCodes[index].city != search.city) {
            trouve = false;
        }
    }
    
    response.writeHead(200, {
        'content-type': 'text/json'
    });

    response.end(trouve ? JSON.stringify(zipCodes[index].zip) : "connais pas");
}