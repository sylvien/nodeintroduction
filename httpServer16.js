// use different packages: mongodb, express
const MongoClient = require('mongodb').MongoClient;
const express = require('express');

// use express
const app = express();

// assign db to url
const url = 'mongodb://localhost:27017/';

// set listenning port
app.listen(8000);

// define route to get all books
app.get('/books', function (request, response) {
    response.setHeader('Content-Type', 'text/json');

    // connect to mongodb
    MongoClient.connect(url, {useNewUrlParser: true, useUnifiedTopology: true},
        function (errors, dbmongo) {
            // connect to Library db
            const dbLibrary = dbmongo.db("Library");
            
            // connect to Books collection 
            dbLibrary.collection("Books").find({}).toArray(function (err, result) {
                response.send(JSON.stringify(result));
                dbmongo.close();
             });
        });

});