// use different packages: mongodb, express
const MongoClient = require('mongodb').MongoClient;
const express = require('express');

// use express
const app = express();

// assign db to url
const url = 'mongodb://localhost:27017/';

// set listenning port
app.listen(8000);

// define route to get all zipcodes
app.get('/zipcodes', function (request, response) {
    response.setHeader('Content-Type', 'text/json');

    // connect to mongodb
    MongoClient.connect(url, {useNewUrlParser: true, useUnifiedTopology: true},
        function (errors, dbmongo) {
            // connect to Address db
            const dbAddress = dbmongo.db("Address");
            
            // connect to Books collection 
            dbAddress.collection("Zipcodes").find({}).toArray(function (err, result) {
                response.send(JSON.stringify(result));
                dbmongo.close();
             });
        });
});

// define route to get all cities
app.get('/cities', function (request, response) {
    response.setHeader('Content-Type', 'text/json');

    // connect to mongodb
    MongoClient.connect(url, {useNewUrlParser: true, useUnifiedTopology: true},
        function (errors, dbmongo) {
            // connect to Address db
            const dbAddress = dbmongo.db("Address");
            
            // connect to Books collection 
            dbAddress.collection("Zipcodes").find({}, {projection: {_id: 0, zip:0, lng:0, lat:0}}).toArray(function (err, result) {
                response.send(JSON.stringify(result));
                dbmongo.close();
             });
        });
});