// f8-lancer f9-stop (ctrl+s f8)
// http://127.0.0.1:8080/zipcode?city=bruxelles PUIS f8 sur le code

// créer une variable qui « requiert » le package ‘http’
const http = require("http");

// instancier le serveur :
const server = http.createServer(NewClientCallback);

// préciser le port TCP sur lequel le serveur va écouter
server.listen(8080);

// envoyer dans la console une ligne pour stipuler que le serveur est démarré :
console.log("Server running at http://127.0.0.1:8080/");

function NewClientCallback(request, response) {
    console.log("New Client Connection");
    // console.log(request.headers);
    // console.log(request.headers["accept-language"]);
    // console.log(request.url);

    response.writeHead(200, {'content-type': 'text/html'});

    if(request.headers["accept-language"].substring(0, 2) === "en"){

        response.end("<h1>Hello</h1>");
    }
    else{

        response.end("<h1>Salut</h1>");
    }

    // response.end("<h1>Hello world</h1>");
}