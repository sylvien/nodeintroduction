const http = require("http");

let rawData = "";

const requestToSend = {
    host: "api.irail.be",
    port: 80,
    path: "/liveboard/?id=BE.NMBS.008812005&lang=fr&format=json"
};

http.get(requestToSend, receiveResponseCallback);

// data et endsont 2 mots clés d'événements
function receiveResponseCallback(responseFromServer) {
    responseFromServer.on("data", chunk => {
        rawData += chunk;
    });
    responseFromServer.on("end", function (chunk) {
        console.log(JSON.parse(rawData)["station"]);
        
        const dataJson = JSON.parse(rawData);
        console.log(dataJson.station);

        const date = new Date(dataJson.timestamp * 1000);
        console.log(date.getHours() + ":" + date.getMinutes() + ":" + date.getSeconds());
        console.log(" ");

        const nbDeparts = dataJson.departures.number;

        for (let index = 0; index < nbDeparts; index++) {
            const depart = "";
            const destination = dataJson.departures.departure[index].station;
            const heure = new Date(dataJson.departures.departure[index].time);
            // console.log(heure);
            // depart += heure.getHours();
            // depart += ":";
            // depart += heure.getMinutes();
            const voie = dataJson.departures.departure[index].platform;
            console.log("Train vers " + destination + "\n \tdépart prévu à " + depart + "\n \tvoie : " + voie);            
        }
        // console.log(rawData);
    });

    // console.log(responseFromServer);
    // console.log(responseFromServer["_readableState"]);
}